#pragma once

#include <concepts>
#include <complex>

namespace linalg::num {
	
    #pragma region Fundamental number types
    
    using Float64  = double;
    using Float32  = float;

    using Int64    = signed long long int;
    using Int32    = signed int;
    using Int16    = signed short;
    using Int8     = signed char;

    using UInt64   = unsigned long long int;
    using UInt32   = unsigned int;
    using UInt16   = unsigned short;
    using UInt8    = unsigned char;
	
    #pragma endregion

    #pragma region Complex number traits

    template<typename>
    struct __is_complex_float_helper
            : public std::false_type { };

    template<>
    struct __is_complex_float_helper<std::complex<Float32>>
    : public std::true_type { };

    template<>
    struct __is_complex_float_helper<std::complex<Float64>>
    : public std::true_type { };


    template<typename>
    struct __is_complex_int_helper
            : public std::false_type { };

    template<>
    struct __is_complex_int_helper<std::complex<Int64>>
            : public std::true_type { };

    template<>
    struct __is_complex_int_helper<std::complex<Int32>>
            : public std::true_type { };

    template<>
    struct __is_complex_int_helper<std::complex<Int16>>
            : public std::true_type { };

    template<>
    struct __is_complex_int_helper<std::complex<Int8>>
            : public std::true_type { };

    /// is_complex_float
    template<typename T>
    struct is_complex_float
            : public __is_complex_float_helper<typename std::remove_cv_t<T>>::type { };

    /// is_complex_int
    template<typename T>
    struct is_complex_int
            : public __is_complex_int_helper<typename std::remove_cv_t<T>>::type { };

    #pragma endregion

    #pragma region Number Concepts
	
    template<typename T>
    concept Integer = std::is_integral<T>::value;
	
    template<typename T>
    concept Signed = Integer<T> && std::is_signed<T>::value;

    template<typename T>
    concept UnSigned = Integer<T> && !std::is_signed<T>::value;

    template<typename T>
    concept Float = std::is_floating_point<T>::value;

    template<typename T>
    concept Real = Integer<T> || Float<T>;

    template<typename T>
    concept ComplexInteger = is_complex_int<T>::value;

    template<typename T>
    concept ComplexFloat = is_complex_float<T>::value;

    template<typename T>
    concept Complex = ComplexInteger<T> || ComplexFloat<T>;

    template<typename T>
    concept Number = Real<T> || Complex<T>;

	template<typename T>
    concept is_rational = requires (T q)
    {
        q.num;
        q.den;
    };
	
    template <Integer Int>
    struct Rational
    {
        Int num;
        Int den;
        Rational(Int a, Int b) : num(a), den(b){}
    };
	
    #pragma endregion
   
	#pragma region Number promotion rules

    template <Number A, Number B>
    struct __is_convertible_to_helper : std::false_type {};

    template <Float A, Float B>
    requires (sizeof(A) <= sizeof(B))
        struct __is_convertible_to_helper<A, B> : std::true_type {};

    template <Integer A, Integer B>
    requires (sizeof(A) <= sizeof(B))
        struct __is_convertible_to_helper<A, B> : std::true_type {};

    template <Integer A, Float B>
    requires (sizeof(A) <= sizeof(B))
        struct __is_convertible_to_helper<A, B> : std::true_type {};

    template <typename A, typename B>
    concept is_convertible_to = __is_convertible_to_helper<A, B>::value;

    template<Real S, Real T>
    struct __promote { };

    template<Real T>
    struct __promote<T,T>
    {
        using type = T;
    };
	
    template<Real S, Real T> requires is_convertible_to<S, T>
    struct __promote<S, T>
    {
        using type = T;
    };

    template<Real S, Real T> requires is_convertible_to<T, S>
    struct __promote<S, T>
    {
        using type = S;
    };

    template<Real S, Real T>
    using Promote = typename __promote<S, T>::type;

    template<Real S, Real T>
    auto inline promote(T x)
    {
        return static_cast<Promote<S, T>>(x);
    }

    template<typename S, typename T>
    concept is_statically_convertible_to = std::same_as<S, Promote<S, T>>;

	#pragma endregion 

	#pragma region Extension methods for numbers

    // machine epsilon corresponding to type T
    template <Real T>
    constexpr auto eps() { return 2 * std::numeric_limits<T>::epsilon(); }

    // Compute if two real
    static bool within_tolerance(const Real auto& x, const  Real auto& y, const Float auto& epsilon) {
        return std::abs(x - y) < epsilon;
    }

    // Compute if two real
    static bool within_tolerance(const Complex auto& x, const Complex auto& y, const Float auto& epsilon) {
        return std::abs(x - y) < epsilon;
    }

    template<Real S, Real T>
    static bool within_tolerance(const S& x, const T& y) {
        return std::abs(x - y) < eps<Promote<S, T>>();
    }

    template<Complex C1, Complex C2>
    static bool within_tolerance(const C1& x, const C2& y) {
        using T1 = typename C1::value_type;
        using T2 = typename C2::value_type;
        return std::abs(x - y) < eps<Promote<T1, T2>>();
    }

	#pragma endregion
	
	#pragma region Number conversion rules

    // Throw an InExactError if conversion cannot be done within machine tolerance
    struct InexactError : public std::exception {
        [[nodiscard]] const char* what() const throw () {
            return "ERROR: InexactError";
        }
    };
	
    // Static conversion, possible if type can be statically converted to without loss
    template<Real S, Real T> requires is_statically_convertible_to<S, T>
    auto convert(const T& x) { return static_cast<S>(x); }

    // Dynamic conversion, needed if static conversion is not possible. Throw an 
    // InexactError if conversion leads to loss of accuracy.
    template<Real S, Real T>
    S convert(const T& x)
    {
        S y = static_cast<S>(x);
    	if (!within_tolerance(x, y))
            throw InexactError();

    	return y;
    }
    
	#pragma endregion 

}