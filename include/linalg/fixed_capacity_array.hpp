#pragma once

#include <assert.h>
#include <array>


namespace linalg {

// array class that allocates once and cannot change its capacity
// after initialization.
template <typename T> class fixed_capacity_array {

public:

  using value_type = T;

  T *m_data = nullptr;

  std::size_t m_capacity;
  std::size_t m_size;

public:
  fixed_capacity_array() : m_capacity(0), m_size(0) {}

  fixed_capacity_array(std::size_t capacity) : m_capacity(capacity), m_size(0) {
    m_data = new T[capacity];
  }

  fixed_capacity_array(const fixed_capacity_array<T> &other) noexcept { 
    copy(other);
  }

  fixed_capacity_array(fixed_capacity_array<T> &&other) noexcept {
    move(other);
  }

  fixed_capacity_array<T> &operator=(const fixed_capacity_array<T> &other) noexcept {
    if (&other != this) {
      delete[] m_data;
      m_data = nullptr;
      copy(other);
    }
    return *this;
  }

  fixed_capacity_array<T> &operator=(fixed_capacity_array<T> &&other) noexcept {
    move(other);
    return *this;
  }

  ~fixed_capacity_array() { delete[] m_data; }

  std::size_t size() const { return m_size; }
  std::size_t capacity() const { return m_capacity; }

  void initialize(std::size_t size) {
    assert(size <= m_capacity);
    m_size = size;
    for (int index = 0; index < m_size; ++index) {
      m_data[index] = T(0);
    }
  }

  void initialize() { initialize(m_capacity); }

  auto &push_back(T value) {
    m_data[m_size] = value;
    ++m_size;
    return m_data[m_size - 1];
  }

  const T &operator[](std::size_t index) const {
    assert(index < m_size);
    return m_data[index];
  }

  T &operator[](std::size_t index) {
    assert(index < m_size);
    return m_data[index];
  }

  auto &back() noexcept { return m_data[m_size - 1]; }

  // Begin iterator
  T *begin() const { return m_data; };

  // End iterator
  T *end() const { return m_data + m_size; };

private:

  void copy(const fixed_capacity_array<T> &other) {
    m_data = new T[other.capacity()];
    m_size = other.size();
    m_capacity = other.capacity();
    std::copy(m_data, m_data + m_size, other.m_data);
  }

  void move(fixed_capacity_array<T> &other) {
    this->m_data = other.m_data;
    this->m_capacity = other.m_capacity;
    this->m_size = other.m_size;
    other.m_data = nullptr; // reset other.m_data to avoid double deletion
    other.m_size = 0;
  }
};

} // namespace dolphinmesh