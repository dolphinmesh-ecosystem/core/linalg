#pragma once

#include "numbers.hpp"
#include "sequences.hpp"

namespace linalg::comb {

    using namespace num;
    using namespace seq;

	template<Integer S, Integer T>
    S binomial(const T& n, T k) {
        S res = 1;

        // Since C(n, k) = C(n, n-k)
        if (k > n - k)
            k = n - k;

        // Calculate value of
        // [n * (n-1) *---* (n-k+1)] / [k * (k-1) *----* 1]
        for (T i = 0; i < k; ++i) {
            res *= (n - i);
            res /= (i + 1);
        }

        return res;
    }

    template<Integer S, std::ranges::input_range Range >
		requires Integer<value_type<Range>>
    S multinomial(const Range& range){
        using T = value_type<Range>;
		S res = 1;
        for (T n = 0;  T k : range) {
            n += k;
            res *= binomial<S>(n, k);
        }
        return res;
    }
    
    template<Integer S, Integer T>
    S multinomial(const T& a, const T& b) {
        return binomial<S>(a + b, b);
    }
    
    template<Integer S, Integer... Ts>
    S multinomial(const Ts&... ts) {
        return multinomial<S>(static_array(ts...));
    }
    
}