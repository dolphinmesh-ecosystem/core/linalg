#pragma once

#include <cmath>
#include <array>
#include <iostream>
#include <stdexcept>

#include "numbers.hpp"
#include "Fastor/Fastor.h"

namespace linalg::vector{

    using namespace linalg::num;

    template <Number T, int N> class Vector;
    template <Number T, int M, int N> class Matrix;

    /// <summary>
    /// Scalar number type
    /// </summary>
    template <Number T> using Scalar = Fastor::Tensor<T>;


    /// <summary>
    /// Dense vector interface into Fastor::Tensor<T,N>
    /// </summary>
    template <Number T, int N> class Vector : public Fastor::Tensor<T, N> {

      public:

      // inherit all constructors from Fastor::Tensor
      using Fastor::Tensor<T, N>::Tensor;

      // create a vector with all values initialized to the same value
      static Vector<T, N> fill(T value) {
        Fastor::Tensor<T, N> result;
        result.fill(value);
        return result;
      }

      // create a vector of ones
      static Vector<T, N> ones() { 
        Fastor::Tensor<T, N> result;
        result.ones();
        return result;
      }

      // create a vector of zeros
      static Vector<T, N> zeros() { 
        Fastor::Tensor<T, N> result;
        result.zeros();
        return result; 
      }

      // create the indicator vector
      static Vector<T, N> indicator(int k) { 
        auto result = Vector<T, N>::zeros();
        result(k) = 1;
        return result;
      }

      // create the indicator vector
      static Vector<T, N> random() {
        Fastor::Tensor<T, N> result;
        result.random();
        return result; 
      }

      /// <summary>
      /// Compute the norm of the vector
      /// </summary>
      /// <returns></returns>
      T norm() const { return Fastor::norm(*this); }

      /// <summary>
      /// Normalize vector and return result
      /// </summary>
      /// <returns></returns>
      Vector<T, N> normalize() { 
        *this /= this->norm();
        return *this;
      }

      /// <summary>
      /// Compute the dot-product of two vectors
      /// </summary>
      /// <param name="u"></param>
      /// <param name="v"></param>
      /// <returns></returns>
      T dot(const Vector<T,N>& other) const {
        return Fastor::inner(*this, other);
      }

      /// <summary>
      /// Compute the cross product of two vectors
      /// </summary>
      /// <param name="u"></param>
      /// <param name="v"></param>
      /// <returns></returns>
      Vector<T,N> cross(const Vector<T,N>& other) const {
        return Fastor::cross(*this, other);
      }


      /// <summary>
      /// Check if vector is epsilon close to input vector
      /// </summary>
      /// <param name="other"></param>
      /// <param name="epsilon"></param>
      /// <returns></returns>
      const bool within_tolerance(const Vector<T, N> &other, const Float auto &epsilon) const {
        for (int i = 0; i < N; ++i) {
          if (!linalg::num::within_tolerance(this->operator()(i), other(i), epsilon))
            return false;
        }
        return true;
      }

      /// <summary>
      /// Check if vector is in machine accuracy of input vector
      /// </summary>
      /// <param name="other"></param>
      /// <returns></returns>
      const bool within_tolerance(const Vector<T, N> &other) const {
        return this->within_tolerance(other, linalg::num::eps<T>());
      }

    };



    /// <summary>
    /// Dense matrix interface into Fastor::Tensor<T,N>
    /// </summary>
    template <Number T, int M, int N> class Matrix : public Fastor::Tensor<T, M, N> {

    public:

      // inherit all constructors from Fastor::Tensor
      using Fastor::Tensor<T, M, N>::Tensor;

      // create a matrix with all values initialized to the same value
      static Matrix<T, M, N> fill(T value) { 
        Fastor::Tensor<T, M, N> result;
        result.fill(value);
        return result;
      }

      // create a matrix of ones
      static Matrix<T, M, N> ones() { 
        Fastor::Tensor<T, M, N> result;
        result.ones();
        return result;
      }

      // create a matrix of zeros
      static Matrix<T, M, N> zeros() { 
        Fastor::Tensor<T, M, N> result;
        result.zeros();
        return result; 
      }

      // create a matrix of random numbers
      static Matrix<T, M, N> random() {
        Fastor::Tensor<T, M, N> result;
        result.random();
        return result;
      }

      /// <summary>
      /// Compute the norm of the vector
      /// </summary>
      /// <returns></returns>
      T norm() const { return Fastor::norm(*this); }

      /// <summary>
      /// Check if matrix is epsilon close to input matrix
      /// </summary>
      /// <param name="other"></param>
      /// <param name="epsilon"></param>
      /// <returns></returns>
      const bool within_tolerance(const Matrix<T, M, N> &other,
                                  const Float auto &epsilon) const {
        Matrix<T, M, N> A = *this - other;
        return linalg::num::within_tolerance(A.norm(), 0.0, epsilon);
      }

      /// <summary>
      /// Check if matrix is in machine accuracy of input matrix
      /// </summary>
      /// <param name="other"></param>
      /// <returns></returns>
      const bool within_tolerance(const Matrix<T, M, N> &other) const {
        return within_tolerance(other, 0.0);
      }

      /// <summary>
      /// Return the transpose of this matrix
      /// </summary>
      /// <returns></returns>
      inline Matrix<T, N, M> transpose() const { return Fastor::transpose(*this); }
   
      ///// <summary>
      ///// Matrix-matrix multiplication
      ///// </summary>
      ///// <param name="other"></param>
      ///// <returns></returns>
      //template <int K> 
      //inline Matrix<T, M, K> operator*(const Matrix<T, N, K>& other) const {
      //  return Fastor::matmul(*this, other);
      //}

      ///// <summary>
      ///// Matrix-vector multiplication
      ///// </summary>
      ///// <param name="other"></param>
      ///// <returns></returns>
      //inline Vector<T, M> operator*(const Vector<T, N> &other) const {
      //  return Fastor::matmul(*this, other);
      //}
    };


    /// <summary>
    /// Type alias for three-dimensional vector
    /// </summary>
    template <Number T> using Vector3 = Vector<T, 3>;

    /// <summary>
    /// Type-alias for two-dimensional vector
    /// </summary>
    template <Number T> using Vector2 = Vector<T, 2>;

    /// <summary>
    /// Type-alias for 3x3 matrix
    /// </summary>    
    template <Number T> using Matrix3 = Matrix<T, 3, 3>;

    /// <summary>
    /// Type-alias for 2x3 matrix
    /// </summary>
    template <Number T> using Matrix23 = Matrix<T, 2, 3>;

    /// <summary>
    /// Type-alias for 3x2 matrix
    /// </summary>
    template <Number T> using Matrix32 = Matrix<T, 3, 2>;

    /// <summary>
    /// Type-alias for 1x3 matrix
    /// </summary>
    template <Number T> using Matrix13 = Matrix<T, 1, 3>;

    /// <summary>
    /// Type-alias for 3x1 matrix
    /// </summary>
    template <Number T> using Matrix31 = Matrix<T, 3, 1>;

    /// <summary>
    /// Type-alias for 2x2 matrix
    /// </summary>
    template <Number T> using Matrix2 = Matrix<T, 2, 2>;

    /// <summary>
    /// Type-alias for 1x2 matrix
    /// </summary>
    template <Number T> using Matrix12 = Matrix<T, 1, 2>;

    /// <summary>
    /// Type-alias for 2x1 matrix
    /// </summary>
    template <Number T> using Matrix21 = Matrix<T, 2, 1>;

    /// <summary>
    /// Type-alias for 1x1 matrix
    /// </summary>
    template <Number T> using Matrix1 = Matrix<T, 1, 1>;


    template <typename... Ts>
    constexpr auto static_vector(Ts &&...ts)
        -> Vector<std::common_type_t<Ts...>, sizeof...(ts)> {
      return {std::forward<Ts>(ts)...};
    }

}