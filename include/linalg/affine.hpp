#pragma once

#include <array>
#include <cmath>
#include <iostream>

#include "Fastor/Fastor.h"
#include "numbers.hpp"
#include "dense.hpp"

namespace linalg::affine {

using namespace linalg::num;
using namespace linalg::vector;

/// <summary>
/// Barycentric coordinate vector
/// </summary>
template <Real T> class BarycentricCoord : public Vector3<T> {

public:
  /// <summary>
  ///  General constructor
  /// </summary>
  /// <typeparam name="...Args"></typeparam>
  /// <param name="...args"></param>
  template <typename... Args>
  BarycentricCoord(Args &&...args) : Vector3<T>{std::forward<Args>(args)...} {
    auto *data = static_cast<Vector3<T>>(*this).data();
    assert((std::abs(data[0] + data[1] + data[2] - 1.0) < 1e-12) &&
           "This is not a Barycentric coordinate \n");
  }

  /// <summary>
  /// Construct a Barycentric point from the three coordinates
  /// </summary>
  /// <param name="v"></param>
  BarycentricCoord(T lambda0, T lambda1, T lambda2)
      : Vector3<T>{lambda0, lambda1, lambda2} {
    assert((std::abs(lambda0 + lambda1 + lambda2 - 1.0) < 1e-12) &&
           "This is not a Barycentric coordinate \n");
  }

  /// <summary>
  /// Construct a Barycentric coordinate from a two-dimensional vector
  /// </summary>
  /// <param name="v"></param>
  BarycentricCoord(T lambda1, T lambda2)
      : Vector3<T>{1.0 - lambda1 - lambda2, lambda1, lambda2} {}
};

/// <summary>
/// Barycentric direction vector
/// </summary>
template <Real T> class BarycentricDir : public Vector3<T> {

public:
  /// <summary>
  ///  General constructor
  /// </summary>
  /// <typeparam name="...Args"></typeparam>
  /// <param name="...args"></param>
  template <typename... Args>
  BarycentricDir(Args &&...args) : Vector3<T>{std::forward<Args>(args)...} {
    auto *data = static_cast<Vector3<T>>(*this).data();
    assert((std::abs(data[0] + data[1] + data[2]) < 1e-12) &&
           "This is not a Barycentric direction \n");
  }

  /// <summary>
  /// Construct a Barycentric direction from the three coordinates
  /// </summary>
  /// <param name="v"></param>
  BarycentricDir(T lambda0, T lambda1, T lambda2)
      : Vector3<T>{lambda0, lambda1, lambda2} {
    assert((std::abs(lambda0 + lambda1 + lambda2) < 1e-12) &&
           "This is not a Barycentric diretion \n");
  }

  /// <summary>
  /// Construct a Barycentric direction from a two-dimensional vector
  /// </summary>
  /// <param name="v"></param>
  BarycentricDir(T lambda1, T lambda2)
      : Vector3<T>{-lambda1 - lambda2, lambda1, lambda2} {}
};

/// <summary>
/// Check if vector is an affine coordinate, which means it sums to unity.
/// </summary>
/// <param name="lambda"></param>
/// <returns></returns>
template <Real T> bool isa_affine_coord(const Vector3<T> &lambda) {
  return std::abs(lambda(0) + lambda(1) + lambda(2) - 1.0) < 1e-12;
}

/// <summary>
/// Check if vector is an affine direction, which means it sums to zero.
/// </summary>
/// <param name="lambda"></param>
/// <returns></returns>
template <Real T> bool isa_affine_dir(const Vector3<T> &lambda) {
  return std::abs(lambda(0) + lambda(1) + lambda(2)) < 1e-12;
}


template <Real T> using Point3 = Vector<T, 3>;

template <Real T> using Point2 = Vector<T, 2>;

template <Real T, int N> using Point = Vector<T, N>;


} // namespace linalg::affine