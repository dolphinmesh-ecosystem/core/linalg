#pragma once

#include <ranges>
#include <memory>
#include <iterator>
#include <array>

#include "numbers.hpp"

namespace linalg::seq{

    using namespace num;

	// construct a fixed size array of value_type Ts with variable input arguments
    template<typename... Ts>
    constexpr auto static_array(Ts&&... ts)->std::array<std::common_type_t<Ts...>, sizeof...(ts)>{
        return { std::forward<Ts>(ts)... };
    }

	#pragma region output_iterator_traits
    
    template<class T>
    struct output_iterator_traits : std::iterator_traits<T> {};

    template<class Container>
    struct output_iterator_traits<std::back_insert_iterator<Container>>
        :
        std::iterator<std::output_iterator_tag, typename Container::value_type>
    {};

    template<class Container>
    struct output_iterator_traits<std::front_insert_iterator<Container>>
        :
        std::iterator<std::output_iterator_tag, typename Container::value_type>
    {};

    template<class Container>
    struct output_iterator_traits<std::insert_iterator<Container>>
        :
        std::iterator<std::output_iterator_tag, typename Container::value_type>
    {};

    template <class T, class charT, class traits>
    struct output_iterator_traits<std::ostream_iterator<T, charT, traits>>
        :
        std::iterator<std::output_iterator_tag, T>
    {};

    template <class charT, class traits>
    struct output_iterator_traits<std::ostreambuf_iterator<charT, traits>>
        :
        std::iterator<std::output_iterator_tag, charT>
    {};

	#pragma endregion 
	

	#pragma region value type and concepts for iterators and ranges
    
    template <typename T>
    struct value_type_helper {
        using type = T;
    };

    template <std::input_or_output_iterator It>
    struct value_type_helper<It>
    {
        using type = typename output_iterator_traits<It>::value_type;
    };
    
    template <std::input_iterator It>
    struct value_type_helper<It>
    {
        using type = typename It::value_type;
    };
	
    template <std::ranges::range Range>
    struct value_type_helper<Range>
    {
        using type = typename value_type_helper<std::ranges::iterator_t<Range>>::type;
    };
    
	// Implement value_type_helper<T> to get a simple way to get the value type of an object.
    template <typename T>
    using value_type = typename value_type_helper<T>::type;

    // output_iterator of fixed value type T
    template<typename It, typename T>
    concept output_iterator_fixed_t = std::output_iterator<It, T> && std::is_same<value_type<It>, T>::value;

    // output_range of fixed value type T
    template<typename Range, typename T>
    concept output_range_fixed_t = std::ranges::output_range<Range, T> && std::is_same<value_type<Range>, T>::value;
    
	#pragma endregion 

	//#pragma region reduction functions 
 //   
	//// sum of a range
	//template<std::ranges::input_range InputRange>
 //   auto sum(InputRange& range)
 //   {
 //       using T = value_type<InputRange>;
 //       return std::accumulate(std::begin(range), std::end(range), T(0));
 //   }

	//// length of a range
 //   auto length(std::ranges::range auto range)
 //   {
 //       return  std::distance(std::begin(range), std::end(range));
 //   }

	//// mean of a float range
 //   template<std::ranges::input_range Range>
	//	requires Float<value_type<Range>>
 //   auto mean(Range range)
 //   {
 //       return sum(range) / length(range);
 //   }

	//// mean of an integer range
 //   template<std::ranges::input_range Range>
	//	requires Integer<value_type<Range>>
 //   auto mean(Range range) -> Float64
 //   {
 //       return sum(range) / promote<Float64>(length(range));
 //   }

	//#pragma endregion
 //       
	//#pragma region functions that return ranges 

 //   // compute the cumulative sum of an iterator
	//template<typename T, std::ranges::input_range InputRange>
 //   cppcoro::recursive_generator<T> cumulative_sum(InputRange range)
 //   {
 //       auto first = std::ranges::begin(range);
 //       auto last = std::ranges::end(range);
 //       T save = 0;
 //       do {
 //           save += *first; ++first;
 //           co_yield save;
 //       } while (first != last);
 //   }
	//
	//// compute the cumulative sum of an iterator
	//template <std::input_iterator InputIt, output_iterator_fixed_t<value_type<InputIt>> OutputIt>
	//	requires Real<value_type<InputIt>>
	//auto cumulative_sum(InputIt first, const InputIt last, OutputIt result)
	//{
 //       if (first == last) { return result; }
 //       
 //       value_type<InputIt> save = 0;
 //       *result = save;
 //       do {
 //           save += *first; ++first;
 //           *result = save; ++result;
 //       } while (first != last);
 //       
 //       return result;
	//}

	//// compute the cumulative sum of a range
 //   template <std::ranges::input_range InputRange, output_range_fixed_t<value_type<InputRange>> OutputRange>
	//auto cumulative_sum(const InputRange& input, OutputRange& result)
	//{
 //           return cumulative_sum(std::begin(input), std::end(input), std::back_inserter(result));
	//}

	//// return an iterator with the cumulative sum
 //   template <
 //       std::input_iterator InputIt1,
	//	std::input_iterator InputIt2,
	//	output_iterator_fixed_t<value_type<InputIt1>> OutputIt>

	//static auto fill_n(InputIt1 first, const InputIt1 last, InputIt2 mult, OutputIt result) {
 //       if (first == last) { return result; }
 //   
 //       do {
 //           std::fill_n(result, *mult, *first);
 //           ++mult;
 //       } while (++first != last);
 //   
 //       return result;
 //   }

	//
 //   // compute the cumulative sum of a range
	//template<
 //       std::ranges::input_range InputRange1,
 //       std::ranges::input_range InputRange2,
 //       output_range_fixed_t<value_type<InputRange1>> OutputRange>

	//auto fill_n(const InputRange1& input, const InputRange2& mult, OutputRange& result)
 //   {
 //       return fill_n(std::begin(input), std::end(input), std::begin(mult) , std::back_inserter(result));
 //   }
 //   
 //   // template <typename InputIterator, typename OutputIterator1, typename OutputIterator2>
 //   // static auto
 //   //     unique(InputIterator first, InputIterator last, OutputIterator1 vals, OutputIterator2 mult) {
 //   //     if (first == last) { return vals; }
 //   //
 //   //     size_t count = 0;
 //   //     auto save = *first;
 //   //     while (first != last) {
 //   //         if ((*first != save)) {+-
 //   //             *vals = save;  // update current unique value
 //   //             *mult = count; // update current multiplicity
 //   //
 //   //             ++vals;
 //   //             ++mult;
 //   //             count = 0;
 //   //             save = *first;
 //   //         }
 //   //         ++count;
 //   //         ++first;
 //   //     }
 //   //     *vals = save;  ++vals; // update last unique value
 //   //     *mult = count; ++mult; // update last multiplicity
 //   //
 //   //     return vals;
 //   // }

	//#pragma endregion

}