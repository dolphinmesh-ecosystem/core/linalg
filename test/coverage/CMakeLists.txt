# perform code coverage on gnu or clang
if(CMAKE_CXX_COMPILER_ID MATCHES "GNU|Clang")

# Add required flags (GCC & LLVM/Clang)
    target_compile_options(linalg INTERFACE
        -O0        # no optimization
        -g         # generate debug info
        --coverage # sets all required flags
    )

    target_link_libraries(linalg INTERFACE --coverage)
    
endif(CMAKE_CXX_COMPILER_ID MATCHES "GNU|Clang")