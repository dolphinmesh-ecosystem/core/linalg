#include "linalg/numbers.hpp"
#include "test.h"

using namespace linalg::num;

namespace linalg {

    TEST(numbers, size) {
    	EXPECT_EQ(sizeof(Float64), 8);
        EXPECT_EQ(sizeof(Float32), 4);
        EXPECT_EQ(sizeof(Int64), 8);
        EXPECT_EQ(sizeof(Int32), 4);
        EXPECT_EQ(sizeof(Int16), 2);
        EXPECT_EQ(sizeof(Int8), 1);
    }
	
    TEST(within_tolerance, floating_point) {
        const Float64 a = 0.0;
        const Float64 b = 1.0e-12;
        EXPECT_TRUE(num::within_tolerance(a, b, 1.0e-11));
        EXPECT_FALSE(num::within_tolerance(a, b, 1.0e-13));
        EXPECT_FALSE(num::within_tolerance(a, b));
    }

    TEST(within_tolerance, floating_point_int) {
        const Float64 a = 1.0 + 1.0e-12;
        const Int64 b = 1;
        EXPECT_TRUE(num::within_tolerance(a, b, 1.0e-11));
        EXPECT_FALSE(num::within_tolerance(a, b, 1.0e-13));
        EXPECT_FALSE(num::within_tolerance(a, b));
    }

    TEST(within_tolerance, complex_vals) {
        const std::complex<Float64> a (1.0 + 1e-12, 2.0 + 1e-12);
        const std::complex<Float64> b (1.0,2.0);
        EXPECT_TRUE(num::within_tolerance(a, b, 1.0e-11));
        EXPECT_FALSE(num::within_tolerance(a, b, 1.0e-13));
        EXPECT_FALSE(num::within_tolerance(a, b));
    }

    TEST(numbers, nested_types) {
        EXPECT_TRUE((num::is_convertible_to<Float32, Float32>));
        EXPECT_TRUE((num::is_convertible_to<Float64, Float64>));
        EXPECT_TRUE((num::is_convertible_to<Float32, Float64>));
        EXPECT_FALSE((num::is_convertible_to<Float64, Float32>));
        
        EXPECT_TRUE((num::is_convertible_to<Int32, Int32>));
        EXPECT_TRUE((num::is_convertible_to<Int64, Int64>));
        EXPECT_TRUE((num::is_convertible_to<Int32, Int64>));
        EXPECT_FALSE((num::is_convertible_to<Int64, Int32>));
        
        EXPECT_TRUE((num::is_convertible_to<Int64, Float64>));
        EXPECT_TRUE((num::is_convertible_to<Int32, Float32>));
        EXPECT_FALSE((num::is_convertible_to<Float64, Int64>));
        EXPECT_FALSE((num::is_convertible_to<Float32, Int32>));
        
        EXPECT_FALSE((num::is_convertible_to<Int64, Float32>));
    }

    TEST(numbers, promotion) {
        EXPECT_TRUE(typeid(Promote<Int64, Int64>) == typeid(Int64));
        EXPECT_TRUE(typeid(Promote<Int32, Int64>) == typeid(Int64));
        EXPECT_TRUE(typeid(Promote<Int64, Int32>) == typeid(Int64));
        EXPECT_TRUE(typeid(Promote<Int32, Int32>) == typeid(Int32));

    	EXPECT_TRUE(typeid(Promote<Float64, Int64>) == typeid(Float64));
        EXPECT_TRUE(typeid(Promote<Int64, Float64>) == typeid(Float64));
        
        EXPECT_TRUE(typeid(promote<Int64>(1.2)) == typeid(Float64));
        EXPECT_TRUE(typeid(promote<Float64>(1)) == typeid(Float64));
    }

    TEST(numbers, conversion) {
        EXPECT_TRUE(convert<Int64>(1.0) == 1);
        EXPECT_TRUE(convert<Int64>(static_cast<Int32>(1)) == static_cast<Int32>(1));
        EXPECT_TRUE(convert<Float64>(static_cast<Float32>(1.2)) == static_cast<Float32>(1.2));
        EXPECT_TRUE(convert<Float32>(1.0) == static_cast<Float32>(1.0));
        EXPECT_TRUE(convert<Int64>(1.0 + 0.5*eps<Float64>()) == 1);
        EXPECT_THROW(convert<Int64>(1.0 + 3 * eps<Float64>()), InexactError);
    }
	
    TEST(numbers, rational) {
        auto q = Rational(1, 10);
        EXPECT_TRUE( is_rational<decltype(q)> );
        EXPECT_FALSE(is_rational<decltype(1)> );
    }

}