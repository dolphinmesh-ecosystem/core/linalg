#include "test.h"

#include "linalg/affine.hpp"

namespace linalg::affine {


TEST(affine, barycentric_coord) {
 
  BarycentricCoord<double> x = {0.5, 0.2, 0.3};
  Vector2<double> f{0.2, 0.3};
  auto y = BarycentricCoord(0.0,0.5);
  auto z = BarycentricCoord(0.2, 0.4, 0.4);
  EXPECT_TRUE(isa_affine_coord(x));
}

} // namespace linalg::vector