# Find gtest
find_package(GTest REQUIRED)

# code coverage
option(CODE_COVERAGE "Enable coverage reporting" OFF)
if(CODE_COVERAGE)
	message("Building with code coverage ON")
	add_subdirectory(coverage)
endif(CODE_COVERAGE)

include(CTest)
include(GoogleTest)

# local test executable
add_executable(test_numbers "numbers.cpp")
target_link_libraries(test_numbers PRIVATE linalg GTest::GTest)
gtest_discover_tests(test_numbers)

# local test executable
add_executable(test_dense "dense.cpp")
target_link_libraries(test_dense PRIVATE linalg GTest::GTest)
gtest_discover_tests(test_dense)

# local test executable
add_executable(test_arrays "arrays.cpp")
target_link_libraries(test_arrays PRIVATE linalg GTest::GTest)
gtest_discover_tests(test_arrays)

# local test executable
add_executable(test_affine "affine.cpp")
target_link_libraries(test_affine PRIVATE linalg GTest::GTest)
gtest_discover_tests(test_affine)

add_executable(console "main.cpp")
target_link_libraries(console PRIVATE linalg)