#include <test.h>

#include "linalg/numbers.hpp"
#include "linalg/sequences.hpp"
#include <ranges>

using namespace numbers;

namespace sequences {
    
    class range_methods : public testing::Test {
    public:
        std::vector<Float64> sequence_int, sequence_float;
        std::vector<Int64> mult;
        size_t size;
        range_methods(){
            sequence_int = std::vector<Float64>({1,2,3,4});
            sequence_float = std::vector<Float64>({1.0,2.0,3.0,4.0});
            mult = std::vector<Int64>({2,1,3,2});
            size = sum(mult);
        }
    };
	
    TEST_F(range_methods, sum)
	{;
		EXPECT_TRUE(numbers::within_tolerance(sum(sequence_float), 10.0));
	}
	
    TEST_F(range_methods, length) {
        EXPECT_EQ(length(sequence_float), 4);
    }

	TEST_F(range_methods, mean) {
        EXPECT_TRUE(numbers::within_tolerance(mean(sequence_float), 2.5));
    }


    TEST_F(range_methods, output_iterator_traits) {
        auto v = std::vector<Float64>{ 1,2,3,4 };
        auto u = std::back_inserter(v);
    
        using Range = decltype(v);
        using It = decltype(u);
        
        EXPECT_TRUE((std::same_as<output_iterator_traits<It>::value_type, Float64>));
        EXPECT_TRUE((std::same_as<value_type<It>, Float64>));
        EXPECT_TRUE((std::same_as<value_type<Range>, Float64>));
    }
	   
    TEST_F(range_methods, concepts) {
        auto v = std::vector<Float64>{ 1,2,3,4 };
        auto u = std::back_inserter(v);
        
        using Range = decltype(v);
        using OutputRange = decltype(u);
    	using It = decltype(v.begin());
    	
    	// This is correct behavior
        EXPECT_TRUE((std::output_iterator<It, Float64>));
       
        // This behavior is unwanted
    	EXPECT_TRUE((std::output_iterator<It, Float32>));
        EXPECT_TRUE((std::output_iterator<It, Int64>));
     
        // The new output iterator is of fixed type and behaves as desired
        EXPECT_TRUE((output_iterator_fixed_t<It, Float64>));
        EXPECT_FALSE((output_iterator_fixed_t<It, Float32>));
        EXPECT_FALSE((output_iterator_fixed_t<It, Int64>));
    	
    	//The new output iterator is of fixed type and behaves as desired
        EXPECT_TRUE((output_range_fixed_t<Range, Float64>));
        EXPECT_FALSE((output_range_fixed_t<Range, Float32>));
        EXPECT_FALSE((output_range_fixed_t<Range, Int64>));
    }
	   
    TEST_F(range_methods, cumulative_iterator) {
        std::vector<Float64> cumsum;   
        cumulative_sum(std::begin(sequence_float), std::end(sequence_float), std::back_inserter(cumsum));
        
        EXPECT_EQ(cumsum.size(), 5);
        EXPECT_EQ(cumsum[0], 0);
        EXPECT_EQ(cumsum[1], 1);
        EXPECT_EQ(cumsum[2], 3);
        EXPECT_EQ(cumsum[3], 6);
        EXPECT_EQ(cumsum[4], 10);
    }
    
    TEST_F(range_methods, cumulative_range) {
        std::vector<Float64> cumsum;
        cumulative_sum(sequence_int,cumsum);
    
        EXPECT_EQ(cumsum.size(), 5);
        EXPECT_EQ(cumsum[0], 0);
        EXPECT_EQ(cumsum[1], 1);
        EXPECT_EQ(cumsum[2], 3);
        EXPECT_EQ(cumsum[3], 6);
        EXPECT_EQ(cumsum[4], 10);
    }

    TEST_F(range_methods, fill_n) {
        std::vector<Float64> v;
        sequences::fill_n(std::begin(sequence_float), std::end(sequence_float), std::begin(mult), std::back_inserter(v));
    
        EXPECT_EQ(v.size(), size);
        EXPECT_EQ(v[0], 1);
        EXPECT_EQ(v[1], 1);
        EXPECT_EQ(v[2], 2);
        EXPECT_EQ(v[3], 3);
        EXPECT_EQ(v[4], 3);
        EXPECT_EQ(v[5], 3);
        EXPECT_EQ(v[6], 4);
        EXPECT_EQ(v[7], 4);
    }
	
    TEST_F(range_methods, fill_n_range){
        std::vector<Float64> v;
        sequences::fill_n(sequence_float, mult, v);
    
        EXPECT_EQ(v.size(), size);
        EXPECT_EQ(v[0], 1);
        EXPECT_EQ(v[1], 1);
        EXPECT_EQ(v[2], 2); 
        EXPECT_EQ(v[3], 3);
        EXPECT_EQ(v[4], 3);
        EXPECT_EQ(v[5], 3);
        EXPECT_EQ(v[6], 4);
        EXPECT_EQ(v[7], 4);
    }
    

    // class range_unique : public testing::Test {
    // public:
    //     std::vector<Int64> sequence;
    //     std::vector<Int64> u;
    //     std::vector<Int64> m;

    //     range_unique(){
    //         sequence = std::vector<Int64>({1, 1, 2, 3, 3, 5, 5, 5, 6});
    //         size_t n = sequence.size();
    //         u.reserve(n);
    //         m.reserve(n);
    //     }
    // };

    // TEST_F(range_unique, double_output_with_back_inserter){
    //     auto uend = Range::unique(sequence.begin(), sequence.end(), std::back_inserter(u), std::back_inserter(m));

    //     EXPECT_EQ(u.size(), 5);
    //     EXPECT_EQ(m.size(), 5);

    //     EXPECT_EQ(u[0], 1);
    //     EXPECT_EQ(u[1], 2);
    //     EXPECT_EQ(u[2], 3);
    //     EXPECT_EQ(u[3], 5);
    //     EXPECT_EQ(u[4], 6);

    //     EXPECT_EQ(m[0], 2);
    //     EXPECT_EQ(m[1], 1);
    //     EXPECT_EQ(m[2], 2);
    //     EXPECT_EQ(m[3], 3);
    //     EXPECT_EQ(m[4], 1);
    // }

    // TEST_F(range_unique, double_output_with_iterator){
    //     u = std::vector<Int64>(5);
    //     m = std::vector<Int64>(5);

    //     auto uend = Range::unique(sequence.begin(), sequence.end(), u.begin(), m.begin());

    //     EXPECT_EQ(u[0], 1);
    //     EXPECT_EQ(u[1], 2);
    //     EXPECT_EQ(u[2], 3);
    //     EXPECT_EQ(u[3], 5);
    //     EXPECT_EQ(u[4], 6);
    //     EXPECT_TRUE(uend == u.end());

    //     EXPECT_EQ(m[0], 2);
    //     EXPECT_EQ(m[1], 1);
    //     EXPECT_EQ(m[2], 2);
    //     EXPECT_EQ(m[3], 3);
    //     EXPECT_EQ(m[4], 1);
    // }

}