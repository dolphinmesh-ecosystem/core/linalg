#include "linalg/dense.hpp"
#include <iostream>

using namespace linalg;
using namespace linalg::num;
using namespace linalg::vector;

int main() {

  Vector3<double> a{1, 2, 3};
  assert(a.size() == 3);
  assert(a(0) == 1);
  assert(a(1) == 2);
  assert(a(2) == 3);

  return 0;
}