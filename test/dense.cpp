#include "test.h"

#include "linalg/dense.hpp"

namespace linalg::vector {

    TEST(dense, static_vector_size2) {
      Vector2<double> a{1, 2};
      EXPECT_EQ(a.size(), 2);
      EXPECT_EQ(a[0], 1);
      EXPECT_EQ(a[1], 2);
    }

    TEST(dense, static_vector_size3) { 
      Vector3<double> a{1, 2, 3};
      EXPECT_EQ(a.size(), 3);
      EXPECT_EQ(a[0], 1);
      EXPECT_EQ(a[1], 2);
      EXPECT_EQ(a[2], 3);
    }

    TEST(dense, vector) { 
      Vector3<double> a = {3, 4, 0};

      // test indexing
      EXPECT_EQ(a(0), 3);
      EXPECT_EQ(a(1), 4);
      EXPECT_EQ(a(2), 0);

      // test operations in multiple vectors
      EXPECT_EQ(a.norm(), 5);
      EXPECT_EQ(a.dot(a), 25);
      EXPECT_EQ(a.cross(a).norm(), 0);
    }

    TEST(vector, within_tolerance) {
      Vector3<double> a = {3, 4, 0};
      Vector3<double> b = {3, 4, 0};

      // test indexing
      EXPECT_TRUE(a.within_tolerance(b, 1e-12));
      EXPECT_TRUE(a.within_tolerance(b));

      Vector3<double> c = {3.0 + 1e-11, 4.0, 0.0};
      EXPECT_FALSE(a.within_tolerance(c, 1e-12));
      EXPECT_TRUE(a.within_tolerance(c, 1e-10));
    }

    TEST(vector, zeros) {
      auto x = Vector3<double>::zeros();

      EXPECT_EQ(x(0), 0);
      EXPECT_EQ(x(1), 0);
      EXPECT_EQ(x(2), 0);
    }

    TEST(vector, ones) {
      auto x = Vector3<double>::ones();

      EXPECT_EQ(x(0), 1);
      EXPECT_EQ(x(1), 1);
      EXPECT_EQ(x(2), 1);
    }

    TEST(vector, fill) {
      auto x = Vector3<double>::fill(2.0);

      EXPECT_EQ(x(0), 2);
      EXPECT_EQ(x(1), 2);
      EXPECT_EQ(x(2), 2);
    }

    TEST(vector, indicator) {
      auto x = Vector3<double>::indicator(1);

      EXPECT_EQ(x(0), 0);
      EXPECT_EQ(x(1), 1);
      EXPECT_EQ(x(2), 0);
    }

    TEST(vector, normalize) {
      auto x = Vector3<double>{3,4,5};
      auto y = x / std::sqrt(50);

      x.normalize();
      EXPECT_TRUE(x.within_tolerance(y));
    }

    TEST(matrix, transpose) {
      
      auto A = Matrix23<double>::random();
      auto B = A.transpose();

      EXPECT_TRUE(B.dimension(0) == 3);
      EXPECT_TRUE(B.dimension(1) == 2);
      
      EXPECT_EQ(A(0, 0), B(0, 0));
      EXPECT_EQ(A(0, 1), B(1, 0));
      EXPECT_EQ(A(0, 2), B(2, 0));

      EXPECT_EQ(A(1, 0), B(0, 1));
      EXPECT_EQ(A(1, 1), B(1, 1));
      EXPECT_EQ(A(1, 2), B(2, 1));
    }

}