
#include "linalg/fixed_capacity_array.hpp"
#include "test.h"
#include <concepts>


namespace linalg {

class test_fixed_capacity_array : public ::testing::Test {

public:
  fixed_capacity_array<double> x;

protected:
  void SetUp() override {
    x = fixed_capacity_array<double>(10);
    x.push_back(1.0);
    x.push_back(2.0);
    x.push_back(3.0);
    x.push_back(4.0);
  }
};

TEST_F(test_fixed_capacity_array, copy_constructor) { 
  auto y = fixed_capacity_array(x);

  EXPECT_EQ(y.size(), x.size());
  EXPECT_EQ(y.capacity(), x.capacity());

  for (int i = 0; i < x.size(); ++i) {
    EXPECT_EQ(x[i], y[i]);
  }

  EXPECT_FALSE(x.m_data == y.m_data);
}

TEST_F(test_fixed_capacity_array, copy_assignment) {
  auto y = fixed_capacity_array<double>(10); // making a copy
  y.push_back(10.0);
  y.push_back(20.0);
  double* dataref = y.m_data;

  // copy assignment
  y = x;

  // check that a copy has been made
  EXPECT_EQ(y.size(), x.size());
  EXPECT_EQ(y.capacity(), x.capacity());
  EXPECT_FALSE(x.m_data == y.m_data);

  for (int i = 0; i < x.size(); ++i) {
    EXPECT_EQ(x[i], y[i]);
  }
}

TEST_F(test_fixed_capacity_array, move_constructor) {
  auto y = fixed_capacity_array(std::move(x)); // move construction

  EXPECT_TRUE(x.m_data==nullptr);
  EXPECT_TRUE(x.size() == 0);

  EXPECT_EQ(y.size(), 4);
  EXPECT_EQ(y[0], 1.0);
  EXPECT_EQ(y[1], 2.0);
  EXPECT_EQ(y[2], 3.0);
  EXPECT_EQ(y[3], 4.0);
}

TEST_F(test_fixed_capacity_array, move_assignment) {
  fixed_capacity_array<double> y;
  y = std::move(x);

  EXPECT_TRUE(x.m_data == nullptr);
  EXPECT_TRUE(x.size() == 0);

  EXPECT_EQ(y.size(), 4);
  EXPECT_EQ(y[0], 1.0);
  EXPECT_EQ(y[1], 2.0);
  EXPECT_EQ(y[2], 3.0);
  EXPECT_EQ(y[3], 4.0);
}

TEST_F(test_fixed_capacity_array, initialize) { 
    x.initialize();
    EXPECT_EQ(x.size(), 10);
    for (auto value : x) {
      EXPECT_EQ(value, 0);
    }
}

TEST_F(test_fixed_capacity_array, size_and_capacity) {
  EXPECT_TRUE((std::same_as<decltype(x)::value_type, double>));
  EXPECT_EQ(x.size(), 4);
  EXPECT_EQ(x.capacity(), 10);
}

TEST_F(test_fixed_capacity_array, assignment) {
  EXPECT_EQ(x[0], 1.0);
  EXPECT_EQ(x[1], 2.0);
  EXPECT_EQ(x[2], 3.0);
  EXPECT_EQ(x[3], 4.0);
}

TEST_F(test_fixed_capacity_array, range) {
    double k = 1.0;
    for (auto v : x) {
        EXPECT_EQ(v, k);
        ++k;
    }
}

} // linalg