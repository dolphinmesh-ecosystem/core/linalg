#include "linalg/combinatorics.hpp"
#include "test.h"

#include "linalg/dense.hpp"

namespace linalg {

    using namespace num;
    using namespace comb;

    TEST(combinatorics, binomial) {
        EXPECT_EQ(binomial<UInt32>(6, 4), 15);
    }

    TEST(combinatorics, multinomial_length_2) {
        EXPECT_EQ(multinomial<UInt32>(2, 4), 15);
    }
    
    TEST(combinatorics, multinomial_length_3) {
        EXPECT_EQ(multinomial<UInt32>(2, 3, 4), 1260);
    }
	   
    TEST(combinatorics, multinomial_length_4){
        EXPECT_EQ(multinomial<UInt64>(7, 9, 4, 6), 12760912164000);
    }

}