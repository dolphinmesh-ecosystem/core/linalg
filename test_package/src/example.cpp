#include "linalg/dense.hpp"
#include "linalg/fixed_capacity_array.hpp"
#include "linalg/numbers.hpp"
#include <iostream>

using namespace linalg;
using namespace linalg::num;
using namespace linalg::vector;

int main() {

  // create and print fixed_capacity_array<int>
  auto x = linalg::fixed_capacity_array<int>(10);
  x.push_back(1);
  x.push_back(2);
  x.push_back(3);
  x.push_back(4);

  std::cout << "entries in fixed_capacity_array<int> are: " << std::endl;
  for (auto value : x) {
    std::cout << value << std::endl;
  }
  std::cout << std::endl;

  // create and print Vector<double>
  Vector3<double> a{1, 2, 3};
  assert(a.size() == 3);
  assert(a(0) == 1);
  assert(a(1) == 2);
  assert(a(2) == 3);

  std::cout << "entries in Vector<double> are: " << std::endl;
  std::cout << a(0) << std::endl;
  std::cout << a(1) << std::endl;
  std::cout << a(2) << std::endl;
  std::cout << std::endl;

  // is a Int32 convertible to a Int64?
  std::cout << "num::is_convertible_to<Int32, Int64> is "
            << num::is_convertible_to<Int32, Int64> << std::endl;

  return 0;
}