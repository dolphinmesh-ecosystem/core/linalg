from conans import ConanFile, CMake

class TestPkgConan(ConanFile):
    name = "linalg"
    version = "0.4.0"

    # Optional metadata
    license = "Proprietary"
    author = "Rene Hiemstra (rene.r.hiemstra@gmail.com)"
    url = "https://gitlab.com/dolphinmesh-ecosystem/linalg"
    description = "Linear algebra routines wrapped from Fastor and Eigen"
    topics = ("Linear algebra", "Fastor", "Eigen")

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "external/CMakeLists.txt", "include/*"
    generators = "cmake_find_package"

    no_copy_source = True

    def requirements(self):
        self.requires("gtest/cci.20210126")
        self.requires("eigen/3.4.0")
        
    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        
    def package(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.install()

    def package_info(self):
        self.info.header_only()